import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Publication } from '../../models/publication';
import { GLOBAL } from '../../services/global';
import { UserService } from '../../services/user.service';
import { PublicationService } from '../../services/publication.service';

@Component({
  selector: 'timeline',
  templateUrl: './timeline.component.html',
  providers: [UserService, PublicationService],
})
export class TimelineComponent implements OnInit {
  public title: string;
  public identity;
  public token;
  public url: string;
  public status!: string;
  public page;
  public total: any;
  public pages: number | undefined;
  public itemsPerPage: any;
  public publications!: Publication[];
  public showImage!: string;

  constructor(
    private _userService: UserService,
    private _publicationService: PublicationService
  ) {
    this.title = 'Timeline';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.page = 1;
  }

  ngOnInit() {
    console.log('timeline.component cargado correctamente!!');
    this.getPublications(this.page);
  }

  getPublications(page: number | undefined, adding = false) {
    this._publicationService.getPublications(this.token, page).subscribe(
      (response: {
        publications: Publication[];
        total_items: any;
        pages: any;
        items_per_page: any;
      }) => {
        if (response.publications) {
          this.total = response.total_items;
          this.pages = response.pages;
          this.itemsPerPage = response.items_per_page;

          if (!adding) {
            this.publications = response.publications;
          } else {
            var arrayA = this.publications;
            var arrayB = response.publications;
            this.publications = arrayA.concat(arrayB);

            // $('html, body').animate(
            //   { scrollTop: $('body').prop('scrollHeight') },
            //   500
            // );
          }

          if (page && this.pages && page > this.pages) {
            //this._router.navigate(['/home']);
          }
        } else {
          this.status = 'error';
        }
      },
      (error: any) => {
        var errorMessage = <any>error;
        console.log(errorMessage);
        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    );
  }

  public noMore = false;
  viewMore() {
    this.page += 1;

    if (this.page == this.pages) {
      this.noMore = true;
    }

    this.getPublications(this.page, true);
  }

  refresh() {
    this.getPublications(1);
  }

  showThisImage(id: any) {
    this.showImage = id;
  }

  hideThisImage() {
    this.showImage = "0";
  }

  deletePublication(id: any) {
    this._publicationService.deletePublication(this.token, id).subscribe(
      () => {
        this.refresh();
      },
      (error: any) => {
        console.log(<any>error);
      }
    );
  }
}
