import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrincipalComponent } from './principal/principal.component';
import { LoginComponent } from './seguridad/login/login.component';
import { RegisterComponent } from './seguridad/register/register.component';
import { SeguridadRouter } from './seguridad/security.router';
import { ChatComponent } from './chat/chat.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FollowingComponent } from './components/following/following.component';
import { FollowedComponent } from './components/followed/followed.component';

import { UserGuard } from './services/user.guard';

const routes: Routes = [
  { path: '', component: PrincipalComponent, canActivate: [SeguridadRouter] },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'home', component: HomeComponent },
  { path: 'mis-datos', component: UserEditComponent, canActivate: [UserGuard] },
  { path: 'gente', component: UsersComponent, canActivate: [UserGuard] },
  { path: 'gente/:page', component: UsersComponent, canActivate: [UserGuard] },
  { path: 'timeline', component: TimelineComponent, canActivate: [UserGuard] },
  { path: 'perfil/:id', component: ProfileComponent, canActivate: [UserGuard] },
  {
    path: 'siguiendo/:id/:page',
    component: FollowingComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'seguidores/:id/:page',
    component: FollowedComponent,
    canActivate: [UserGuard],
  },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [SeguridadRouter],
})
export class AppRoutingModule {}
