import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { PrincipalComponent } from './principal/principal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { RegisterComponent } from './seguridad/register/register.component';
import { LoginComponent } from './seguridad/login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TopMenuComponent } from './navegacion/topMenu/topMenu.component';
import { SideMenuComponent } from './navegacion/sideMenu/sideMenu.component';
import { SecurityService } from './seguridad/security.service';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SecurityInterceptor } from './seguridad/security-interceptor';
import { ChatComponent } from './chat/chat.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { PublicationsComponent } from './components/publications/publications.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FollowingComponent } from './components/following/following.component';
import { FollowedComponent } from './components/followed/followed.component';

// import { HomeComponent } from 'home-component/home.component';
// import { UserEditComponent } from 'user-edit_component/user-edit.component';
// import { UsersComponent } from 'users_component/users.component';
// import { TimelineComponent } from 'timeline_component/timeline.component';
// import { PublicationsComponent } from 'publications_component/publications.component';
// import { ProfileComponent } from 'profile_component/profile.component';
// import { FollowingComponent } from 'following_component/following.component';
// import { FollowedComponent } from 'followed_component/followed.component';

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    RegisterComponent,
    LoginComponent,
    TopMenuComponent,
    SideMenuComponent,
    ChatComponent,
    HomeComponent,
    UserEditComponent,
    UsersComponent,
    TimelineComponent,
    ProfileComponent,
    FollowingComponent,
    FollowedComponent,
    PublicationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: SecurityInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    SecurityService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
